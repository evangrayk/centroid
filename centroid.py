#!/usr/bin/env python
#-*- coding: utf-8 -*-
# Evan Krause, 2014
from PIL import Image, ImageDraw
from images2gif import writeGif
import os, sys
from math import cos, sin, fabs, atan2, sqrt

def brightness(pixel):
    return pixel[0] + pixel[1] + pixel[2]


def getRotatedPosition(theta, x, y, cx, cy):
    """ input: a point (x,y), a center (cx, cy) and an angle (Radians!).
        output: the adjusted point created by rotating about that angle """
    x2 = x - cx
    y2 = y - cy
    c = cos(theta)
    s = sin(theta)
    x3 = x2 * c - y2 * s
    y3 = x2 * s + y2 * c
    x3 += cx
    y3 += cy
    return (int(x3), int(y3))


def drawPoint(p, r, drawer, fill=(255,255,255)):
    """ draws a point at the given position """
    drawer.ellipse([p[0] - r, p[1] - r, p[0] + r, p[1] + r], fill)


def findLineCenter(outline, x1, y1):
    """ given an image and a start point,
        find a point that is in the middle of the line
    """
    xmin = x1
    xminF = False
    xmax = x1
    xmaxF = False

    ymin = y1
    yminF = False
    ymax = y1
    ymaxF = False

    while (True):
        # check each direction start at x1,y1, 
        if not xminF:
            if (outOfBounds(outline, xmin, y1)):
                xminF = True
            elif brightness(outline.getpixel((xmin, y1))) > 10:
                xmin -= 1
            else:
                xminF = True

        if not xmaxF:
            if (outOfBounds(outline, xmax, y1)):
                xmaxF = True
            elif brightness(outline.getpixel((xmax, y1))) > 10:
                xmax += 1
            else:
                xmaxF = True

        if not yminF:
            if (outOfBounds(outline, x1, ymin)):
                yminF = True
            elif brightness(outline.getpixel((x1, ymin))) > 10:
                ymin -= 1
            else:
                yminF = True

        if not ymaxF:
            if (outOfBounds(outline, x1, ymax)):
                ymaxF = True
            elif brightness(outline.getpixel((x1, ymax))) > 10:
                ymax += 1
            else:
                ymaxF = True
        # if either direction finds max and min as black, 
        # return centered at that point
        if xminF and xmaxF:
            return ((xmax + xmin) / 2, y1)
        if yminF and ymaxF:
            return (x1, (ymax + ymin) / 2)

    return (x1, y1)


def outOfBounds(outline, x, y):
    return (x >= outline.size[0] or y >= outline.size[1] or x < 0 or y < 0)


def checkLine(outline, p1, p2):
    """ check every pixel from p1 to p2. If any point in the line drawn is 
        a nonzero pixel in outline, return that point.
        else, return (-1, -1) as none found
        Uses Bresenham's line algorithm
    """
    dX = fabs(p2[0] - p1[0])
    dY = fabs(p2[1] - p1[1])
    x = p1[0]
    y = p1[1]
    offsetY = 0
    offsetX = 0
    counter = 0

    if (p1[0] > p2[0]):
        offsetX = -1;
    elif (p2[0] > p1[0]):
        offsetX = 1;
    else:
        offsetX = 0;

    if (p1[1] > p2[1]):
        offsetY = -1;
    elif (p2[1] > p1[1]):
        offsetY = 1;
    else:
        offsetY = 0;

    if (dX > dY):
        error = dX / 2
        while (x != p2[0]):
            counter += 1
            error -= dY
            if (error < 0):
                y += offsetY
                error += dX
            x += offsetX
            # check if this is a pixel in the outline
            if (outOfBounds(outline, x, y)):
                break
            if (brightness(outline.getpixel((x, y))) > 0):
                return (x, y)
    elif (dY > dX):
        error = dY / 2
        while (y != p2[1]):
            counter += 1
            error -= dX
            if (error < 0):
                x += offsetX
                error += dY
            y += offsetY
            # check if this is a pixel in the outline
            if (outOfBounds(outline, x, y)):
                break
            if (brightness(outline.getpixel((x, y))) > 0):
                return (x, y)
    elif (dX == dY):
        while x != p2[0]:
            x += 1
            y += 1
            if (outOfBounds(outline, x, y)):
                break
            if (brightness(outline.getpixel((x, y))) > 0):
                return (x, y)
    return (-1, -1)


def polygonFromOutline(outline, map):
    """ Given an outline of an image, try to generate a polygon
        of the outline
        input: PIL Image of outline, white on black
    """
    r = 20 # distance between points
    x = outline.size[0] - r
    y = 0
    # find a spot on (probably) left side of shape
    while (True):
        if (outOfBounds(outline, x, y)):
            print "[ERROR] Out of bounds! (%i, %i)" % (x,y)
            break
        if (brightness(outline.getpixel((x, y))) > 10):
            break
        if (y < outline.size[1] - r):
            y += r
        else:
            x -= r
            y = 0
    x, y = findLineCenter(outline, x, y)
    sx, sy = x, y
    lastX, lastY = x, y
    points = [(x,y)]
    count = 1
    i = 0
    halfpi = 3.1415926 / 2 
    startAngle = -1.0 * halfpi # start going upward

    while (True):
        i += 1
        """ start at a point on the outline
            go up by radius. go out and down alternating left and right, searching for more outline. Find center of line near that point.
            find angle between this and last point, and go forward in that direction for the next iteration
        """
        # 
        if (count > 500):
            print "Too many points! stopping..."
            break
        if (count > 3 and fabs(sx - x) < r and fabs(sy - y) < r):
            #print "Reached end!" # ended normally
            break
        # start at last point offset by r in startAngle
        p1 = getRotatedPosition(startAngle, x + r, y, x, y) # forward
        pL = getRotatedPosition(startAngle - halfpi, x + r, y, x, y) # "left" (CCW)
        pR = getRotatedPosition(startAngle + halfpi, x + r, y, x, y) # "right" (CW)
        p0 = getRotatedPosition(startAngle + 3.1415926, x + r, y, x, y) # forward

        # debug draw lines
        #drawer = ImageDraw.Draw(map)
        #drawer.line([p1, pL], fill=(0, 100, 0))
        #drawer.line([p1, pR], fill=(0, 100, 0))
        #drawer.line([pL, p0], fill=(0, 100, 0))
        #drawer.line([pR, p0], fill=(0, 100, 0))
        #drawPoint((x, y), 2, drawer)
        #del drawer

        p = checkLine(outline, p1, pR)
        if (p == (-1, -1)):
            p = checkLine(outline, p1, pL)
        if (p == (-1, -1)):
            p = checkLine(outline, pL, p0)
        if (p == (-1, -1)):
            p = checkLine(outline, pR, p0)

        count += 1
        if (p != (-1, -1)):
            startAngle = atan2((-1 * y+p[1]), (-1 * x+p[0]))
            x, y = findLineCenter(outline, p[0], p[1])
            #x, y = p
            #print "Adding point (%i, %i)" % (x, y)
            points.append((x, y))
            #outdraw = ImageDraw.Draw(outline)
            #drawPoint((x, y), 1, outdraw, fill=(100, 255, 255 - max(count, 20)))
            #del outdraw
            #map.save("debug/%i.bmp" % i)

            if (fabs(x - lastX) < 0.19*r and fabs(y - lastY) < 0.19*r):
                print "Breaking because point is took close to last point "
                break
            lastX = x
            lastY = y
        else:
            print "Can't find another point! Probably r is too big"
            return points

    return points


def findCentroid(polygon):
    """ Given a polygon (with p[0] == p[-1]), find the centroid
        using modified green's theorem. Also prints area
        because we already know it and it is interesting
    """
    area = 0
    for i in range(len(polygon) - 2):
        (x1, y1) = polygon[i]
        (x2, y2) = polygon[i + 1]
        area += 0.5 * (x1*y2 - x2*y1)
    print "Area: %f" % fabs(area)
    ########
    xbar = 0
    ybar = 0
    for i in range(len(polygon) - 2):
        (x1, y1) = polygon[i]
        (x2, y2) = polygon[i + 1]
        xbar += ((1 / (6*area)) * (x1 + x2) * (x1*y2 - x2*y1))
        ybar += ((1 / (6*area)) * (y1 + y2) * (x1*y2 - x2*y1))
    return (xbar, ybar)


if __name__ == '__main__':
    #open files
    basename = "centroid_"
    if (len(sys.argv) < 3):
        print "Expects 2 arguments: /path/to/outline.BMP /path/to/map.BMP"
        print "    Outline is image with white everywhere except outline to be traced"
        print "    Map is corresponding location to anchor outline to"
        sys.exit(0)
    else:
        print sys.argv[1]
        print sys.argv[2]
        outline = Image.open(sys.argv[1])
        basename = (sys.argv[2])[:-4]
        map = Image.open(sys.argv[2])
    if not os.path.exists(basename):
        os.makedirs(basename)

    # convert to polygon
    polygon = polygonFromOutline(outline, map)
    polygon.append(polygon[0]) # make iteration easy

    # draw the found polygon
    drawer = ImageDraw.Draw(map)
    for i in range(0, len(polygon) - 1):
        drawer.line([polygon[i], polygon[i + 1]], fill=(0, 0, 255))
        #drawPoint(polygon[i], 2, drawer) # draw point

    #find centroid of polygon
    (xbar, ybar) = findCentroid(polygon)
    print "Centroid: (%f, %f)" % (xbar, ybar)
    drawPoint((xbar, ybar), 30, drawer, fill=(0, 0, 0))
    del drawer

    #map.show()
    map.save(basename + "_centroid.BMP")

