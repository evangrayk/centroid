#!/usr/bin/env python
#-*- coding: utf-8 -*-
# Evan Krause, 2014
from PIL import Image, ImageDraw
from images2gif import writeGif
import os, sys
from math import cos, sin

def getRotatedPosition(theta, x, y, cx, cy):
    """ input: a point (x,y), a center (cx, cy) and an angle.
        output: the adjusted point created by rotating about that angle """
    x2 = x - cx
    y2 = y - cy
    theta2 = (3.1415926 * theta) / 180.0
    c = cos(theta2)
    s = sin(theta2)
    x3 = x2 * c - y2 * s
    y3 = x2 * s + y2 * c
    x3 += cx
    y3 += cy
    return (int(x3), int(y3))


def drawPoint(p, r, drawer, fill=255):
    """ draws a point at the given position """
    drawer.ellipse([p[0] - r, p[1] - r, p[0] + r, p[1] + r], fill)


def rotateToFindCentralCurve(campus, outline, basename):
    """ rotate outline through 90 degrees and generate new curve.
        input: Image campus, Image outline, str basename
        return: Image newcampus (campus with new outline drawn on it)
                 Image newoutline (red on black outline of new curve)
                 radius of new outline

        will generate and export gifs of outline as it rotates and 
        campus as it is drawn on """

    mask = outline.copy()
    blank = mask.point(lambda i: 0)
    drawBlank = ImageDraw.Draw(blank) # sorry, drawing a blank on good variable name for this
    outlineFrames = []
    campusFrames  = []

    maxX, minX = campus.size[0], 0
    maxY, minY = campus.size[1], 0
    maxR = 0

    oldCenterCampus = (0, 0)
    theta = 0.0
    dtheta = 90.0 / 90.0
    while (theta < 90.0):
        # rotate outline to crop
        out = mask.rotate(theta)
        # get bounding box and crop to it
        # l, u, r, d are bounds for rotated image
        l, u, r, d = out.getbbox()
        out = out.crop((l, u, r, d))
        # prepare to draw center to full map
        c2 = campus.copy()

        # rotated positions of bounding box
        l2, u2 = getRotatedPosition(theta, l, u, c2.size[0] / 2.0, c2.size[1] / 2.0)
        # These are not the droids you're looking for
        r2, d2 = getRotatedPosition(theta, r, d, c2.size[0] / 2.0, c2.size[1] / 2.0)

        # determine if this bounding box is bigger than previous radiuses
        if (out.size[0] > maxR):
            maxR = out.size[0]	
        if (out.size[1] > maxR):
            maxR = out.size[1]	

        # prepare to draw
        drawCampus = ImageDraw.Draw(c2)
        drawOutline = ImageDraw.Draw(out)

        # center of the image
        cx = c2.size[0] / 2.0
        cy = c2.size[1] / 2.0

        # find centers to draw points at
        centerCampus = (0.5 * (r2 + l2), 0.5 * (d2 + u2))
        centerOut = (out.size[0] / 2.0, out.size[1] / 2.0)

        if (oldCenterCampus != (0, 0)):
            # draw onto different images
            drawCampus.line([centerCampus, oldCenterCampus], fill=255)
            drawPoint(centerOut, 4, drawOutline)
            drawBlank.line([centerCampus, oldCenterCampus], fill=255)

        oldCenterCampus = centerCampus

        # cleanup this iteration
        del drawCampus
        del drawOutline
        campus = c2

        #out.save("%s/Outline%i.BMP" % (directory, theta/dtheta))
        outlineFrames.append(out.copy())

        #c2.save("%s/Campus%i.BMP" % (directory, theta/dtheta))
        campusFrames.append(c2.copy())

        theta = theta + dtheta

    del drawBlank

    radius = max(maxX - minX, maxY - minY)

    # writeGif currently has a bug in that it doesn't work with PIL
    #writeGif(basename + "/outline.gif", outlineFrames)
    #writeGif(basename + "/campus.gif", campusFrames)
    return (campus, blank, maxR)


def method2():
    basename = "rotated_center_"
    if (len(sys.argv) < 3):
        print "Expects 2 arguments: /path/to/outline.BMP /path/to/map.BMP"
        print "    Outline is image with white everywhere except outline to be traced"
        print "    Map is corresponding location to anchor outline to"
        sys.exit(0)
    else:
        print "Outline file: " + sys.argv[1]
    	print "Map file: " + sys.argv[2]
    	outline = Image.open(sys.argv[1])
        basename = (sys.argv[2])[:-4]
    	campus = Image.open(sys.argv[2])
    if not os.path.exists(basename):
            os.makedirs(basename)

    r = 10000
    i = 0
    while (r > 10 and i < 15):
        campus, newoutline, r = rotateToFindCentralCurve(campus, outline, basename)
        print "[Iteration %i] r=%i" % (i, r)
        campus.save(basename + "/campus.bmp")
        newoutline.save(basename + "/outline%i.bmp" % i)
        outline = newoutline
        i += 1



def method1():
    # open image of just outline (to crop)
    # and image of campus (for reference)
    outline = Image.open("UCLAoutline.bmp")
    campus = Image.open("UCLAfull.bmp")
    # all white pixels are now 0, everything else is 255
    mask = outline.point(lambda i: i < 250 and 255)
    out = mask.copy()
    # get bounding box and crop to it
    l, u, r, d = out.getbbox()
    out = out.crop((l, u, r, d))
    # prepare to draw center to full map
    drawCampus = ImageDraw.Draw(campus)
    drawOutline = ImageDraw.Draw(out)
    # find the center point in the cropped outline and full campus
    centerCampus = (l + out.size[0] / 2.0, u + out.size[1] / 2.0)
    centerOut= (out.size[0] / 2.0, out.size[1] / 2.0)
    # radius to draw ellipse with
    radCamp = 5.0
    radOut = 5.0
    #draw at center +/- radius
    drawCampus.ellipse([centerCampus[0] - radCamp, centerCampus[1] - radCamp, centerCampus[0] + radCamp, centerCampus[1] + radCamp], fill=255)
    drawOutline.ellipse([centerOut[0] - radOut, centerOut[1] - radOut, centerOut[0] + radOut, centerOut[1] + radOut], fill=255)
    del drawCampus
    del drawOutline
    #campus.show()
    #out.show()
    out.save("Outline1.BMP")
    campus.save("Campus1.BMP")


if __name__ == '__main__':
    method2()
